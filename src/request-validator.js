import Joi from 'joi';
const pattern = new RegExp("^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$", '')
const transaction = Joi.object({
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      phone: Joi.string().required(),
      bvn: Joi.string().required(),
      dob: Joi.string().pattern(pattern).required(),
      address:Joi.string().required(),
      timeSent: Joi.number().required(),
    }).required();
    

export default  (cb) => async (payload) => {
  try {
     const validate = await transaction.strip(false).validateAsync(payload);
     if (validate) {
      cb(payload, null) 
     } else {
       cb(payload, 'could not validate')
     }
  } catch (err) {
    cb(payload, err.message)
    return false;
  }
};