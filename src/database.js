import { MongoClient } from 'mongodb';
import dotEnv from 'dotenv'
dotEnv.config();

const { CONNECTION_STRING } = process.env;

export default async function () {
  return new Promise((resolve, reject) => {
    MongoClient.connect(CONNECTION_STRING, { 
      useNewUrlParser: true, 
      useUnifiedTopology: true }, (err, client) => {
      if (err) {
        const message = 'Database Connection Error'
        console.error(message);
        console.log(err);
        return reject({ name: 'DatabaseError', reason: err, message});
      } else {
        console.log('Database Started')
        resolve (client)
      }
    })
  })
}
