/* eslint-disable no-undef */
import "regenerator-runtime/runtime.js";

import MongoDbClient from "./database";
import webhook from "./webhook";
import WebSocket from "ws";
const PORT = process.env.PORT || 7003;
MongoDbClient()
  .then((client) => {
    const wss = new WebSocket.Server({ port: PORT });
    const db = client.db(process.env.DATABASE_NAME);
    const server = {
      server: db,
      accounts: db.collection("accounts"),
    };
    wss.on("connection",async function connection(socket) {
      console.log("Connection started");
      socket.io = {};
      socket.io.send = (event, data) => {
        try {
          const payload = JSON.stringify({ event, data })
          return socket.send(payload);
        } catch (e) {
          console.log(e)
        }
      };
      socket.io.listen = (eventName, callback) => {
        socket.onmessage = function (event) {
          const data = JSON.parse(event.data);
          if (data.event === eventName || eventName === '*') {
            callback(data.data);
          }
          // callback(event)
        };
      };
      socket.io.send('DUMP_TRANSACTION', await server.accounts.find().toArray())
      webhook(server, socket);
    });
  })
  .catch((error) => {
    console.log(error);
  });

