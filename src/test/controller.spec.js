/* eslint-disable no-undef */
import supertest from "supertest";
import { assert } from "chai";
import app from "../app";

const request = supertest(app);

describe("APP SERVER /", () => {
  it("server should run successfully", (done) => {
    request.get("/").expect(["powered by EmmsDan"], done);
  });
  it("return 404 when wrong url is entered", (done) => {
    request
      .get("/some-fun-url")
      .expect(["Oops, url not found on this app"], done);
  });
});

// transation test cases