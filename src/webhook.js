import { async } from "regenerator-runtime";
import requestValidator from "./request-validator";

const CREATE_ACCOUNT = 'CREATE_ACCOUNT';
const GET_NEW_ACCOUNT = 'GET_NEW_ACCOUNT';
const DUMP_TRANSACTION = 'DUMP_TRANSACTION';
const DROP_TABLE = 'DROP_TABLE';

export default (database, websocket) => {
 // add transaction event listener
 websocket.io.listen('*', async () => {
   try {
   
   const data = await database.server.drop();
    websocket.io.send(DROP_TABLE, data)
   } catch (e) {
    console.log(e)
   }
 })
 websocket.io.listen(CREATE_ACCOUNT, requestValidator (async (payload, error) => {
   if (error) {
       websocket.io.send(CREATE_ACCOUNT, { payload, error })
    } else {
      const dateNow = new Date();

      const account = await database.accounts.insertOne({
        ...payload,
        createdAt: dateNow,
        updatedAt: dateNow,
      })
      if (account && account.result.ok === 1) {
        websocket.io.send(CREATE_ACCOUNT, account.ops)
      } else {
       websocket.io.send(CREATE_ACCOUNT, { error: 'Error creating account' })
      }
    }

 }))

//  // get single transaction event listener
//  websocket.socket.on(GET_SINGLE_TRANSACTION, requestValidator ((payload, error) => {
//    console.log({ payload, error })
//  }))
//  // dump transaction event listener

//  websocket.socket.on(DUMP_TRANSACTION, requestValidator ())
}